package dtos

type MerchantProposal struct {
	ID            uint64
	Username      string
	Email         string
	DisplayName   string
	Description   string
	Items         []Item
	AdminApproval bool
}
