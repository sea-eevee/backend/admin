package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/backend/admin/repo/repo_customer"
	"gitlab.com/sea-eevee/backend/admin/repo/repo_merchant"
	"gitlab.com/sea-eevee/backend/admin/repo/repo_oms"
	"gitlab.com/sea-eevee/backend/admin/repo/repo_sso"
	"gitlab.com/sea-eevee/backend/admin/uc_admin"
	"gitlab.com/sea-eevee/backend/common/store"
	"log"
	"net/http"
)

func main() {
	repoOMS, err := repo_oms.NewRepoOMS(store.DBConn{
		DBHost: "compfest-marketplace-service-oms-db",
		DBPort: 5432,
		//DBHost: "localhost",
		//DBPort: 13400,
		DBUser: "pgku",
		DBPass: "pgku",
		DBName: "oms",
	})
	if err != nil {
		log.Fatal(err)
	}
	repoMerchant, err := repo_merchant.NewRepoMerchant(store.DBConn{
		DBHost: "compfest-marketplace-service-merchant-db",
		DBPort: 5432,
		//DBHost: "localhost",
		//DBPort: 13200,
		DBUser: "pgku",
		DBPass: "pgku",
		DBName: "merchant",
	})
	if err != nil {
		log.Fatal(err)
	}
	repoCustomer, err := repo_customer.NewRepoCustomer(store.DBConn{
		DBHost: "compfest-marketplace-service-customer-db",
		DBPort: 5432,
		//DBHost: "localhost",
		//DBPort: 13300,
		DBUser: "pgku",
		DBPass: "pgku",
		DBName: "customer",
	})
	if err != nil {
		log.Fatal(err)
	}
	repoSSO, err := repo_sso.NewRepoSSO(store.DBConn{
		DBHost: "compfest-marketplace-service-sso-db",
		DBPort: 5432,
		//DBHost: "localhost",
		//DBPort: 14000,
		DBUser: "pgku",
		DBPass: "pgku",
		DBName: "sso",
	})
	if err != nil {
		log.Fatal(err)
	}
	ucAdmin := uc_admin.NewUCAdmin(uc_admin.Param{
		RepoOMS:      repoOMS,
		RepoSSO:      repoSSO,
		RepoCustomer: repoCustomer,
		RepoMerchant: repoMerchant,
	})
	r := NewRouterAdmin(ucAdmin)
	r.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		tpl, err1 := route.GetPathTemplate()
		met, err2 := route.GetMethods()
		fmt.Println(tpl, err1, met, err2)
		return nil
	})

	http.ListenAndServe(":8100", r)
}
