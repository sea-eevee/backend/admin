package repo_oms

import "database/sql"

func (r repoOMS) TransactionReadOne(transactionID uint64) (*Transaction, error) {

	const query = `SELECT 
       					o.id, o.customer_id, o.merchant_id
					FROM transaction t 
					    JOIN orders o on o.id = t.order_id
					WHERE  t.order_id = $1`

	rows, err := r.db.Query(query, transactionID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	m := new(Transaction)
	for rows.Next() {
		err := rows.Scan(&m.TransactionID, &m.CustomerID, &m.MerchantID)
		if err != nil {
			return nil, err
		}
	}

	return m, nil
}
