package repo_oms

func (r repoOMS) CountItem() (uint64, error) {
	var count uint64
	query := `SELECT count(item_id) FROM item_merchant_stock`

	rows, err := r.db.Query(query)
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		err := rows.Scan(&count)
		if err != nil {
			return 0, err
		}
	}
	return count, nil
}
