package repo_oms

import (
	"database/sql"
	"gitlab.com/sea-eevee/backend/common/store"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_oms . Contract

type Contract interface {
	MerchantItemStockReadOne(merchantID uint64) (*MerchantItemStock, error)
	MerchantItemStockUpdate(merchantID, itemID uint64, stockDiff int) error
	MerchantWalletUpdate(merchantID uint64, walletDiff int) error

	TransactionReadMany() ([]*Transaction, error)
	TransactionReadOne(transactionID uint64) (*Transaction, error)
	TransactionReject(transactionID uint64) error

	OrderGetDetail(orderID uint64) ([]*OrderDetail, error)

	CountTransaction() (uint64, error)
	CountItem() (uint64, error)
	MerchantItemRequest(merchantID uint64, item uint64, quantity uint) error
}

type repoOMS struct {
	db *sql.DB
}

func NewRepoOMS(param store.DBConn) (Contract, error) {
	db, err := store.NewPostgres(param)
	if err != nil {
		return nil, err
	}
	return &repoOMS{
		db: db,
	}, nil
}
