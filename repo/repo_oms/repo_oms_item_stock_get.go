package repo_oms

import "database/sql"

func (r repoOMS) MerchantItemStockReadOne(itemID uint64) (*MerchantItemStock, error) {
	const query = `SELECT item_id, merchant_id, stock 
					FROM item_merchant_stock 
					WHERE item_id = $1`

	row := r.db.QueryRow(query, itemID)
	m := new(MerchantItemStock)
	err := row.Scan(&m.ItemID, &m.MerchantID, &m.Stock)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}

	return m, nil
}
