package repo_oms

import "database/sql"

type Transaction struct {
	TransactionID     uint64 `json:"transaction_id"`
	CustomerID        uint64 `json:"customer_id"`
	MerchantID        uint64 `json:"merchant_id"`
	BankName          string `json:"bank_name"`
	BankAccountNumber string `json:"bank_account_number"`
	AdminApproval     *bool  `json:"admin_approval"`
	TransactionDate   uint64 `json:"transaction_date"`
}

func (r repoOMS) TransactionReadMany() ([]*Transaction, error) {
	var mw []*Transaction

	const query = `SELECT 
       					o.id, o.customer_id, o.merchant_id,
       					t.bank_name, t.bank_account_number, t.transaction_date, t.admin_approval
					FROM transaction t JOIN orders o on o.id = t.order_id
					WHERE  t.admin_approval IS NULL AND o.is_paid = TRUE`

	rows, err := r.db.Query(query)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		m := new(Transaction)
		err := rows.Scan(&m.TransactionID, &m.CustomerID, &m.MerchantID,
			&m.BankName, &m.BankAccountNumber, &m.TransactionDate, &m.AdminApproval)
		if err != nil {
			return nil, err
		}
		mw = append(mw, m)
	}

	return mw, nil
}
