package repo_oms

func (r repoOMS) CountTransaction() (uint64, error) {
	var count uint64
	query := `SELECT count(order_id) FROM transaction WHERE admin_approval IS NULL`

	rows, err := r.db.Query(query)
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		err := rows.Scan(&count)
		if err != nil {
			return 0, err
		}
	}
	return count, nil
}
