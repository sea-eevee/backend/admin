package repo_oms

func (r repoOMS) MerchantItemRequest(merchantID uint64, itemID uint64, quantity uint) error {
	query := "INSERT INTO merchant_request_item(item_id, merchant_id, quantity) VALUES ($1, $2, $3)"
	_, err := r.db.Exec(query, merchantID, itemID, quantity)
	if err != nil {
		return err
	}
	return nil
}
