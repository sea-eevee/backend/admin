package repo_oms

type OrderDetail struct {
	ItemID   uint64
	Quantity uint
}

func (r repoOMS) OrderGetDetail(orderID uint64) ([]*OrderDetail, error) {
	var itemQuantities []*OrderDetail
	query := `SELECT item_id, quantity FROM order_detail WHERE order_id = $1`

	rows, err := r.db.Query(query, orderID)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		iq := new(OrderDetail)
		err := rows.Scan(&iq.ItemID, &iq.Quantity)
		if err != nil {
			return nil, err
		}
		itemQuantities = append(itemQuantities, iq)
	}

	return itemQuantities, nil
}
