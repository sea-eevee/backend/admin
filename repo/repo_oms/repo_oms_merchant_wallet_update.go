package repo_oms

func (r repoOMS) MerchantWalletUpdate(merchantID uint64, walletDiff int) error {

	const queryCheck = `SELECT merchant_id FROM merchant_wallet WHERE merchant_id = $1 LIMIT 1`
	rows, err := r.db.Query(queryCheck, merchantID)
	if err != nil {
		return nil
	}
	if rows.Next() {
		const query = `UPDATE 
    				merchant_wallet 
    				SET real_wallet_balance = real_wallet_balance + $2
    				WHERE merchant_id = $1`

		_, err = r.db.Exec(query, merchantID, walletDiff)
		if err != nil {
			return err
		}
	} else {
		const query = `INSERT INTO merchant_wallet VALUES ($1, $2)`
		_, err = r.db.Exec(query, merchantID, walletDiff)
		if err != nil {
			return err
		}
	}

	return nil
}
