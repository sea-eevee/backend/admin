package repo_merchant

type ItemDetail struct {
	ItemID      uint64 `json:"item_id"`
	MerchantID  uint64 `json:"merchant_id"`
	DisplayName string `json:"display_name"`
	Price       uint   `json:"price"`
	Description string `json:"description"`
	Image       string `json:"image"`
}

type MerchantBank struct {
	ID                uint64
	MerchantID        uint64
	BankName          string
	BankAccountNumber string
}

type Location struct {
	LocationID uint64
	Province   string
	City       string
}

type MerchantProfile struct {
	MerchantID  uint64 `json:"merchant_id"`
	DisplayName string `json:"display_name"`
	LocationID  uint64 `json:"location_id"`
	Description string `json:"description"`
	PhotoURL    string `json:"photo_url"`
}
