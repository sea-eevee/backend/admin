package repo_merchant

func (r repoMerchant) TransactionHistoryCreate(merchantID, transactionID, totalPrice uint64) error {
	query := "INSERT INTO merchant_transaction_history(merchant_id, transaction_id, total_price) VALUES ($1, $2, $3)"
	_, err := r.db.Exec(query, merchantID, transactionID, totalPrice)
	if err != nil {
		return err
	}
	return nil
}
