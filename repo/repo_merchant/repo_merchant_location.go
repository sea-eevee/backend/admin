package repo_merchant

import "database/sql"

func (r repoMerchant) LocationReadOne(locationID uint64) (*Location, error) {
	const query = `SELECT id, province, city
					FROM location 
					WHERE id = $1`

	row := r.db.QueryRow(query, locationID)

	m := new(Location)
	err := row.Scan(&m.LocationID, &m.Province, &m.City)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return m, nil
}
