package repo_merchant

import (
	"database/sql"
	"gitlab.com/sea-eevee/backend/common/store"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_merchant . Contract

type Contract interface {
	ItemReadOne(itemID uint64) (*ItemDetail, error)

	LocationReadOne(locationID uint64) (*Location, error)
	ProfileReaMany() ([]*MerchantProfile, error)
	ProfileCreate(profile *MerchantProfile) error

	TransactionHistoryCreate(merchantID, transactionID, totalPrice uint64) error

	ProfileReadOne(merchantID uint64) (*MerchantProfile, error)
}

type repoMerchant struct {
	db *sql.DB
}

func NewRepoMerchant(param store.DBConn) (Contract, error) {
	db, err := store.NewPostgres(param)
	if err != nil {
		return nil, err
	}
	return &repoMerchant{
		db: db,
	}, nil
}
