package repo_merchant

func (r repoMerchant) ProfileCreate(profile *MerchantProfile) error {
	query := "INSERT INTO merchant_profile(merchant_id) VALUES ($1)"
	_, err := r.db.Exec(query, profile.MerchantID)
	return err
}
