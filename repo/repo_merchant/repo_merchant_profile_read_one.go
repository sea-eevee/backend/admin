package repo_merchant

import "database/sql"

func (r repoMerchant) ProfileReadOne(merchantID uint64) (*MerchantProfile, error) {

	const query = `SELECT merchant_id, display_name, location_id, description, photo_url
					FROM merchant_profile 
					WHERE merchant_id = $1`

	row := r.db.QueryRow(query, merchantID)

	m := new(MerchantProfile)
	err := row.Scan(&m.MerchantID, &m.DisplayName, &m.LocationID, &m.Description, &m.PhotoURL)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return m, nil
}
