package repo_merchant

import "database/sql"

func (r repoMerchant) ItemReadOne(itemID uint64) (*ItemDetail, error) {
	m := new(ItemDetail)

	const query = `SELECT id, display_name, price, description, image
					FROM item_detail 
					WHERE id = $1 LIMIT 1`

	rows, err := r.db.Query(query, itemID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		err := rows.Scan(&m.ItemID, &m.DisplayName, &m.Price, &m.Description, &m.Image)
		if err != nil {
			return nil, err
		}
	}

	return m, nil
}
