package repo_customer

func (r repoCustomer) TransactionHistoryCreate(customerID, transactionID uint64, isAccepted bool) error {
	query := "INSERT INTO customer_transaction_history(customer_id, transaction_id, is_success) VALUES ($1, $2, $3)"
	_, err := r.db.Exec(query, customerID, transactionID, isAccepted)
	if err != nil {
		return err
	}
	return nil
}
