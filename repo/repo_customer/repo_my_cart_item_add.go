package repo_customer

func (r repoCustomer) MyCartItemAdd(userID, itemID uint64, quantity uint) error {
	query := "INSERT INTO customer_shopping_cart(customer_id, item_id, quantity) VALUES($1,$2, $3)"
	_, err := r.db.Exec(query, userID, itemID, quantity)
	if err != nil {
		return err
	}
	return nil
}
