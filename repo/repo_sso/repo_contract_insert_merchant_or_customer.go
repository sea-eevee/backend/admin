package repo_sso

import "fmt"

func (r repoSSO) InsertMerchantOrCustomer(username, password, email, role string) (insertedID uint64, err error) {
	query := fmt.Sprintf("INSERT INTO %s(username, hash_password, email) VALUES ($1, $2, $3) RETURNING id", role)
	err = r.db.QueryRow(query, username, password, email).Scan(&insertedID)
	if err != nil {
		return 0, err
	}
	return insertedID, err
}
