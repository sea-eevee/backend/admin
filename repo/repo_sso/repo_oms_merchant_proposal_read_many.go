package repo_sso

type MerchantProposal struct {
	MerchantID uint64 `json:"merchant_id"`
	Username   string `json:"username"`
	Email      string `json:"email"`
	IsApproved *bool  `json:"is_approved"`
}

func (r repoSSO) MerchantProposalReadMany() ([]*MerchantProposal, error) {
	var merchants []*MerchantProposal

	query := `SELECT id, username, email, admin_approval FROM merchant WHERE admin_approval IS NULL`
	rows, err := r.db.Query(query)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		m := new(MerchantProposal)
		err := rows.Scan(&m.MerchantID, &m.Username, &m.Email, &m.IsApproved)
		if err != nil {
			return nil, err
		}
		merchants = append(merchants, m)
	}
	return merchants, nil
}
