package repo_sso

func (r repoSSO) InsertAdmin(username, password string) (insertedID uint64, err error) {
	query := "INSERT INTO admin(username, hash_password) VALUES ($1, $2) RETURNING id"
	err = r.db.QueryRow(query, username, password).Scan(&insertedID)
	if err != nil {
		return 0, err
	}
	return insertedID, err
}
