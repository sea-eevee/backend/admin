package repo_sso

func (r repoSSO) MerchantProposalAcceptReject(merchantID uint64, isApproved bool) error {
	query := `UPDATE merchant
				SET admin_approval = $2
				WHERE id = $1`

	_, err := r.db.Exec(query, merchantID, isApproved)
	if err != nil {
		return err
	}
	return nil
}
