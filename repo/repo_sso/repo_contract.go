package repo_sso

import (
	"database/sql"
	"gitlab.com/sea-eevee/backend/common/store"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_sso . Contract

type Contract interface {
	MerchantProposalReadMany() ([]*MerchantProposal, error)
	MerchantProposalAcceptReject(merchantID uint64, isApproved bool) error

	CountCustomer() (uint64, error)
	CountNewMerchant() (uint64, error)
}

type repoSSO struct {
	db *sql.DB
}

func NewRepoSSO(param store.DBConn) (Contract, error) {
	db, err := store.NewPostgres(param)
	if err != nil {
		return nil, err
	}
	return &repoSSO{
		db: db,
	}, nil
}
