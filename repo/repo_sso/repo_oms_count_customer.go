package repo_sso

func (r repoSSO) CountCustomer() (uint64, error) {
	var count uint64
	query := `SELECT count(id) FROM customer`

	rows, err := r.db.Query(query)
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		err := rows.Scan(&count)
		if err != nil {
			return 0, err
		}
	}
	return count, nil
}
