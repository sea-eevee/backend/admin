package api_admin

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/backend/admin/common/responder"
	"gitlab.com/sea-eevee/backend/admin/uc_admin"
	"net/http"
	"strconv"
)

const URLKeyMerchantID = "merchant_id"

func MerchantProposalAcceptReject(ucFunc uc_admin.MerchantProposalAcceptRejectUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)[URLKeyMerchantID]
		merchantID, err := strconv.ParseUint(vars, 10, 64)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		param := new(uc_admin.MerchantProposalAcceptRejectParam)
		err = json.NewDecoder(r.Body).Decode(param)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		param.MerchantID = merchantID
		resp, err := ucFunc(param)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
