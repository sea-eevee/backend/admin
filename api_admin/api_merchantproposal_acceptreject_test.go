package api_admin

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sea-eevee/backend/admin/uc_admin"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestMerchantProposalAcceptReject(t *testing.T) {
	sampleMerchantID := uint64(5)
	p := uc_admin.MerchantProposalAcceptRejectParam{
		MerchantID: sampleMerchantID,
		IsApproved: false,
	}
	sampleBody, err := json.Marshal(p)
	if err != nil {
		log.Fatal(err)
	}

	var tts = []struct {
		caseName    string
		handlerFunc uc_admin.MerchantProposalAcceptRejectUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			handlerFunc: func(param *uc_admin.MerchantProposalAcceptRejectParam) (*uc_admin.MerchantProposalAcceptRejectResponse, error) {
				assert.Equal(t, sampleMerchantID, param.MerchantID)
				assert.Equal(t, false, param.IsApproved)
				return &uc_admin.MerchantProposalAcceptRejectResponse{}, nil
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/%d", sampleMerchantID), bytes.NewBuffer(sampleBody))
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
		{
			caseName: "when handlerFunc err return err",
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodGet, "/", bytes.NewBuffer(sampleBody))
				return r
			},
			handlerFunc: func(param *uc_admin.MerchantProposalAcceptRejectParam) (*uc_admin.MerchantProposalAcceptRejectResponse, error) {
				return nil, errors.New("any error")
			},
			result: func(resp *http.Response) {
				assert.NotEqual(t, resp.StatusCode, http.StatusOK)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path(fmt.Sprintf("/{%s:[0-9]+}", "merchant_id")).Handler(MerchantProposalAcceptReject(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		req.AddCookie(&http.Cookie{
			Name:     "TOKEN",
			Value:    "authorization info",
			Path:     "/",
			Expires:  time.Now().Add(1 * time.Hour),
			HttpOnly: true,
		})

		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}
