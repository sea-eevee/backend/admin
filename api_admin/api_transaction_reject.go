package api_admin

import (
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/backend/admin/common/responder"
	"gitlab.com/sea-eevee/backend/admin/uc_admin"
	"net/http"
	"strconv"
)

func TransactionReject(ucFunc uc_admin.TransactionRejectUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)[URLKeyTransactionID]
		transactionID, err := strconv.ParseUint(vars, 10, 64)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		resp, err := ucFunc(&uc_admin.TransactionRejectParam{TransactionID: transactionID})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
