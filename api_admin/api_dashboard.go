package api_admin

import (
	"gitlab.com/sea-eevee/backend/admin/common/responder"
	"gitlab.com/sea-eevee/backend/admin/uc_admin"
	"net/http"
)

func Dashboard(ucFunc uc_admin.DashboardUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		resp, err := ucFunc()
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
