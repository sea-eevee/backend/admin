package main

import (
	"fmt"
	"gitlab.com/sea-eevee/backend/admin/api_admin"
	"gitlab.com/sea-eevee/backend/admin/uc_admin"
	"net/http"

	"github.com/gorilla/mux"
)

func NewRouterAdmin(ucAdmin uc_admin.Contract) *mux.Router {
	r := mux.NewRouter()
	r.PathPrefix("/ping").HandlerFunc(ping)

	r.PathPrefix("/dashboard").Methods(http.MethodGet).
		Handler(api_admin.Dashboard(ucAdmin.Dashboard))

	merchant := r.PathPrefix("/merchant").Subrouter()
	{
		merchant.Methods(http.MethodPut).
			Path(fmt.Sprintf("/{%s:[0-9]+}", "merchant_id")).
			Handler(api_admin.MerchantProposalAcceptReject(ucAdmin.MerchantProposalAcceptReject))
		merchant.Methods(http.MethodGet).
			Handler(api_admin.MerchantProposalShow(ucAdmin.MerchantProposalShow))
	}

	transaction := r.PathPrefix("/transaction").Subrouter()
	{
		transaction.Methods(http.MethodPut).
			Path(fmt.Sprintf("/{%s:[0-9]+}/accept", api_admin.URLKeyTransactionID)).
			Handler(api_admin.TransactionAccept(ucAdmin.TransactionAccept))

		transaction.Methods(http.MethodPut).
			Path(fmt.Sprintf("/{%s:[0-9]+}/reject", api_admin.URLKeyTransactionID)).
			Handler(api_admin.TransactionReject(ucAdmin.TransactionReject))

		transaction.Methods(http.MethodGet).
			Handler(api_admin.TransactionShow(ucAdmin.TransactionShow))

	}
	return r
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong from admin service")
}
