package uc_admin

import (
	"gitlab.com/sea-eevee/backend/admin/repo/repo_sso"
)

type MerchantProposalShowParam struct{}

type MerchantProposalShowResponse struct {
	MerchantProposal []*repo_sso.MerchantProposal `json:"merchant_proposal"`
}

type MerchantProposalShowUCFunc func(param *MerchantProposalShowParam) (*MerchantProposalShowResponse, error)

func (u ucAdmin) MerchantProposalShow(param *MerchantProposalShowParam) (*MerchantProposalShowResponse, error) {
	repoMerchants, err := u.repoSSO.MerchantProposalReadMany()
	if err != nil {
		return nil, err
	}

	return &MerchantProposalShowResponse{MerchantProposal: repoMerchants}, nil
}
