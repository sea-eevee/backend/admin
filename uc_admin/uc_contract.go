package uc_admin

import (
	"gitlab.com/sea-eevee/backend/admin/repo/repo_customer"
	"gitlab.com/sea-eevee/backend/admin/repo/repo_merchant"
	"gitlab.com/sea-eevee/backend/admin/repo/repo_oms"
	"gitlab.com/sea-eevee/backend/admin/repo/repo_sso"
)

type Contract interface {
	MerchantProposalAcceptReject(param *MerchantProposalAcceptRejectParam) (*MerchantProposalAcceptRejectResponse, error)
	MerchantProposalShow(param *MerchantProposalShowParam) (*MerchantProposalShowResponse, error)
	TransactionShow(param *TransactionShowParam) (*TransactionShowResponse, error)
	TransactionAccept(param *TransactionAcceptParam) (*TransactionAcceptResponse, error)
	TransactionReject(param *TransactionRejectParam) (*TransactionRejectResponse, error)

	Dashboard() (*DashboardResponse, error)
}

type ucAdmin struct {
	repoOMS      repo_oms.Contract
	repoSSO      repo_sso.Contract
	repoCustomer repo_customer.Contract
	repoMerchant repo_merchant.Contract
}

type Param struct {
	RepoOMS      repo_oms.Contract
	RepoSSO      repo_sso.Contract
	RepoCustomer repo_customer.Contract
	RepoMerchant repo_merchant.Contract
}

func NewUCAdmin(param Param) Contract {
	return ucAdmin{
		repoOMS:      param.RepoOMS,
		repoSSO:      param.RepoSSO,
		repoCustomer: param.RepoCustomer,
		repoMerchant: param.RepoMerchant,
	}
}
