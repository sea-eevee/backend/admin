package uc_admin

type TransactionAcceptParam struct {
	TransactionID uint64
}
type TransactionAcceptResponse struct{}

type TransactionAcceptUCFunc func(param *TransactionAcceptParam) (*TransactionAcceptResponse, error)

func (u ucAdmin) TransactionAccept(param *TransactionAcceptParam) (*TransactionAcceptResponse, error) {
	t, err := u.repoOMS.TransactionReadOne(param.TransactionID)
	if err != nil {
		return nil, err
	}
	orderDetails, err := u.repoOMS.OrderGetDetail(t.TransactionID)
	if err != nil {
		return nil, err
	}

	totalPrice := uint(0)
	for _, od := range orderDetails {
		itemDetail, err := u.repoMerchant.ItemReadOne(od.ItemID)
		if err != nil {
			return nil, err
		}
		totalPrice += itemDetail.Price
	}
	err = u.repoOMS.MerchantWalletUpdate(t.MerchantID, int(totalPrice))
	if err != nil {
		return nil, err
	}

	for _, od := range orderDetails {
		err := u.repoOMS.MerchantItemStockUpdate(t.MerchantID, od.ItemID, int(-od.Quantity))
		if err != nil {
			return nil, err
		}
		err = u.repoOMS.MerchantItemRequest(t.MerchantID, od.ItemID, od.Quantity)
		if err != nil {
			return nil, err
		}
	}

	err = u.repoCustomer.TransactionHistoryCreate(t.CustomerID, param.TransactionID, true)
	if err != nil {
		return nil, err
	}
	err = u.repoMerchant.TransactionHistoryCreate(t.MerchantID, param.TransactionID, uint64(totalPrice))
	if err != nil {
		return nil, err
	}
	return nil, nil
}
