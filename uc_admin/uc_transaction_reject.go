package uc_admin

type TransactionRejectParam struct {
	TransactionID uint64 `json:"transaction_id"`
}
type TransactionRejectResponse struct{}

type TransactionRejectUCFunc func(param *TransactionRejectParam) (*TransactionRejectResponse, error)

func (u ucAdmin) TransactionReject(param *TransactionRejectParam) (*TransactionRejectResponse, error) {
	t, err := u.repoOMS.TransactionReadOne(param.TransactionID)
	if err != nil {
		return nil, err
	}

	orderDetails, err := u.repoOMS.OrderGetDetail(t.TransactionID)
	if err != nil {
		return nil, err
	}
	for _, od := range orderDetails {
		err := u.repoOMS.MerchantItemStockUpdate(t.MerchantID, od.ItemID, int(od.Quantity))
		if err != nil {
			return nil, err
		}
	}

	err = u.repoCustomer.TransactionHistoryCreate(t.CustomerID, param.TransactionID, false)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
