package uc_admin

import (
	"gitlab.com/sea-eevee/backend/admin/repo/repo_customer"
	"gitlab.com/sea-eevee/backend/admin/repo/repo_merchant"
)

type TransactionShowParam struct{}

type TransactionOne struct {
	TransactionID   uint64                         `json:"transaction_id"`
	TransactionDate uint64                         `json:"transaction_date"`
	Customer        *repo_customer.CustomerProfile `json:"customer"`
	Merchant        *repo_merchant.MerchantProfile `json:"merchant"`
	Items           []*repo_merchant.ItemDetail    `json:"items"`
}

type TransactionShowResponse struct {
	Transactions []*TransactionOne `json:"transactions"`
}

type TransactionShowUCFunc func(param *TransactionShowParam) (*TransactionShowResponse, error)

func (u ucAdmin) TransactionShow(param *TransactionShowParam) (*TransactionShowResponse, error) {
	var transactionDtos []*TransactionOne
	transactions, err := u.repoOMS.TransactionReadMany()
	if err != nil {
		return nil, err
	}

	for _, t := range transactions {
		customer, err := u.repoCustomer.ProfileSee(t.CustomerID)
		if err != nil {
			return nil, err
		}

		merchant, err := u.repoMerchant.ProfileReadOne(t.MerchantID)
		if err != nil {
			return nil, err
		}

		orderDetails, err := u.repoOMS.OrderGetDetail(t.TransactionID)
		if err != nil {
			return nil, err
		}

		var items []*repo_merchant.ItemDetail
		for _, od := range orderDetails {
			item, err := u.repoMerchant.ItemReadOne(od.ItemID)
			if err != nil {
				return nil, err
			}
			items = append(items, item)
		}
		transactionDtos = append(transactionDtos, &TransactionOne{
			TransactionID:   t.TransactionID,
			TransactionDate: t.TransactionDate,
			Customer:        customer,
			Merchant:        merchant,
			Items:           items,
		})
	}
	return &TransactionShowResponse{Transactions: transactionDtos}, nil
}
