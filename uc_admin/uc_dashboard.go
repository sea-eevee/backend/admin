package uc_admin

type DashboardResponse struct {
	CountNewCostumer    uint64 `json:"count_new_costumer"`
	CountNewMerchant    uint64 `json:"count_new_merchant"`
	CountNewTransaction uint64 `json:"count_new_transaction"`
	CountProduct        uint64 `json:"count_product"`
}

type DashboardUCFunc func() (*DashboardResponse, error)

func (u ucAdmin) Dashboard() (*DashboardResponse, error) {
	countCustomer, err := u.repoSSO.CountCustomer()
	if err != nil {
		return nil, err
	}
	countMerchant, err := u.repoSSO.CountNewMerchant()
	if err != nil {
		return nil, err
	}
	countTransaction, err := u.repoOMS.CountTransaction()
	if err != nil {
		return nil, err
	}
	countProduct, err := u.repoOMS.CountItem()
	if err != nil {
		return nil, err
	}
	return &DashboardResponse{
		CountNewCostumer:    countCustomer,
		CountNewMerchant:    countMerchant,
		CountNewTransaction: countTransaction,
		CountProduct:        countProduct,
	}, nil
}
