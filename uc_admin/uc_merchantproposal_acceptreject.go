package uc_admin

import "gitlab.com/sea-eevee/backend/admin/repo/repo_merchant"

type MerchantProposalAcceptRejectParam struct {
	MerchantID uint64 `json:"merchant_id"`
	IsApproved bool   `json:"is_approved"`
}

type MerchantProposalAcceptRejectResponse struct{}

type MerchantProposalAcceptRejectUCFunc func(param *MerchantProposalAcceptRejectParam) (*MerchantProposalAcceptRejectResponse, error)

func (u ucAdmin) MerchantProposalAcceptReject(param *MerchantProposalAcceptRejectParam) (*MerchantProposalAcceptRejectResponse, error) {
	err := u.repoSSO.MerchantProposalAcceptReject(param.MerchantID, param.IsApproved)
	if err != nil {
		return nil, err
	}
	if param.IsApproved {
		err := u.repoMerchant.ProfileCreate(&repo_merchant.MerchantProfile{
			MerchantID: param.MerchantID,
		})
		if err != nil {
			return nil, err
		}
	}
	return nil, nil
}
