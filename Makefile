.PHONY : build run-docker clean

network-create:
	docker network create compfest-marketplace

build:
	docker build -t compfest-marketplace-service-admin .

run:
	docker run -d --rm --name compfest-marketplace-service-admin \
	--network=compfest-marketplace \
	-p 8100:8100 \
	compfest-marketplace-service-admin

stop:
	docker stop compfest-marketplace-service-admin || true

app-reload:
	make stop
	make build
	make run

clean:
	docker rmi -f compfest-marketplace-service-admin